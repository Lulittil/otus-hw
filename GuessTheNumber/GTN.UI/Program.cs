using GTN.Domain.Dto;
using GTN.Domain.Interfaces;
using GTN.Domain.Services;
using GTN.Domain.Services.Integer;
using GTN.UI;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddScoped<SettingsService<int>, IntSettingsService>(_ => new IntSettingsService(new SettingsDto<int> { MaxAttempts = 5, MinNumber = 1, MaxNumber = 5}));
builder.Services.AddScoped<ICalculatingService<int>, IntCalculatingService>();
builder.Services.AddScoped<IGeneratingService<int>, IntGeneratingService>();


builder.Services.AddMudServices();

await builder.Build().RunAsync();
