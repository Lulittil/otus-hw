﻿using Microsoft.AspNetCore.Components;
using GTN.Domain.Dto;
using GTN.Domain.Interfaces;
using GTN.Domain.Services;

namespace GTN.UI.Pages;

public partial class Game : ComponentBase
{
    private SettingsDto<int> Settings;
    private bool IsStartButtonDisable { get; set; } = false;
    private int EnteredValue { get; set; } = 0;
    private int NeededNumber { get; set; } = 0;
    private int Attempts { get; set; } = 0;
    private string ResultText { get; set; } = String.Empty;

    [Inject]
    private ICalculatingService<int> _calculateService {  get; set; }

    [Inject]
    private SettingsService<int> _settingsService { get; set; }

    [Inject]
    private IGeneratingService<int> _generateService { get; set; }

    protected override void OnInitialized()
    {
        StartGame();

        base.OnInitialized();
    }

    private void StartGame()
    {
        UpdateData();

        Settings = _settingsService.GetSettings();
        Attempts = Settings.MaxAttempts;
        NeededNumber = _generateService.GenerateNumber();

        StateHasChanged();
    }

    private void UpdateData()
    {
        EnteredValue = 0;
        NeededNumber = 0;
        ResultText = String.Empty;

        StateHasChanged();
    }

    private void ChangingValue(int value)
    {
        EnteredValue = value;

        var res = _calculateService.CalculateResult(NeededNumber, value);
        if (res == GameState.Lower) ResultText = "Загаданное число меньше";
        else if (res == GameState.Upper) ResultText = "Загаданное число больше";
        else if (res == GameState.Win) ResultText = "Вы выиграли!"; 

        Attempts -= 1;

        if (Attempts <= 0) ResultText = "Вы проиграли :с";


        StateHasChanged();
    }

    private void ChangeSettings()
    {
        _settingsService.SetSettings(Settings);
        UpdateData();
        Attempts = Settings.MaxAttempts;
        NeededNumber = _generateService.GenerateNumber();

        StateHasChanged();
    }
}
