﻿public enum GameState
{
    Lower = 0,
    Upper = 1,
    Win = 3,
    Lose = 4,
}