﻿
namespace GTN.Domain.Interfaces;

/// <summary>
/// Сервис для принципа разделения ответственности (расчетный сервис)
/// </summary>
/// <typeparam name="T"></typeparam>
public interface ICalculatingService<T> where T : struct
{
    /// <summary>
    /// Метод для расчета больше или меньше число
    /// </summary>
    /// <param name="generatedNumber">Сгенерированное число</param>
    /// <param name="userNumber">Число, которое ввел пользователь</param>
    /// <returns></returns>
    public GameState CalculateResult(T generatedNumber, T userNumber);
}
