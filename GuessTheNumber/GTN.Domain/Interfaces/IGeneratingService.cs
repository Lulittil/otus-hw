﻿
namespace GTN.Domain.Interfaces;

/// <summary>
/// Сервис для принципа разделения ответственности (сервис генерации)
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IGeneratingService<T> where T : struct
{
    /// <summary>
    /// Метод для генерации числа
    /// </summary>
    /// <returns>Возвращает тип (целое или вещественное)</returns>
    public T GenerateNumber();
}
