﻿
namespace GTN.Domain.Dto;

public struct SettingsDto<T> where T : struct
{
    public int MaxAttempts { get; set; }
    public T MinNumber { get; set; }
    public T MaxNumber { get; set; }
}
