﻿using GTN.Domain.Dto;

namespace GTN.Domain.Services;

/// <summary>
/// Сервис для принципа разделения ответственности (сервис настроек)
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class SettingsService<T> where T : struct
{
    private SettingsDto<T> _settingsDto { get; set; } = new SettingsDto<T>();

    public SettingsService(SettingsDto<T> defaultSettings)
    {
        _settingsDto = defaultSettings;
    }

    /// <summary>
    /// Метод для задания настроек
    /// </summary>
    /// <param name="settings"></param>
    public void SetSettings(SettingsDto<T> settings)
    {
        _settingsDto = settings;
    }

    /// <summary>
    /// Метод для получения настроек
    /// </summary>
    public SettingsDto<T> GetSettings()
    {
        return _settingsDto;
    }
}

