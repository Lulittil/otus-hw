﻿using GTN.Domain.Interfaces;

namespace GTN.Domain.Services.Integer;

public class IntGeneratingService : IGeneratingService<int>
{
    private readonly SettingsService<int> _settings;

    /// <summary>
    /// Принцип DI
    /// </summary>
    /// <param name="settings"></param>
    public IntGeneratingService(SettingsService<int> settings)
    {
        _settings = settings;
    }

    public int GenerateNumber()
    {
        return new Random().Next(_settings.GetSettings().MinNumber, _settings.GetSettings().MaxNumber + 1);
    }
}
