﻿using GTN.Domain.Dto;

namespace GTN.Domain.Services.Integer;

public class IntSettingsService : SettingsService<int>
{
    public IntSettingsService(SettingsDto<int> defaultSettings) : base(defaultSettings)
    {
    }
}
