﻿using GTN.Domain.Interfaces;

namespace GTN.Domain.Services.Integer;

public class IntCalculatingService : ICalculatingService<int>
{

    public GameState CalculateResult(int generatedNumber, int userNumber)
    {
        if (generatedNumber < userNumber) return GameState.Lower;
        else if (generatedNumber > userNumber) return GameState.Upper;
        else return GameState.Win;
    }
}
