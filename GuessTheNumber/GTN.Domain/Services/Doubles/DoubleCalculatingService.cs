﻿
using GTN.Domain.Interfaces;

namespace GTN.Domain.Services;

public class DoubleCalculatingService : ICalculatingService<double>
{

    public GameState CalculateResult(double generatedNumber, double userNumber)
    {
        if (generatedNumber < userNumber) return GameState.Lower;
        else if (generatedNumber > userNumber) return GameState.Upper;
        else return GameState.Win;
    }
}
