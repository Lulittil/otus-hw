﻿using GTN.Domain.Interfaces;

namespace GTN.Domain.Services.Doubles;

public class GeneratingService : IGeneratingService<double>
{
    private readonly SettingsService<double> _settings;

    /// <summary>
    /// Принцип DI
    /// </summary>
    /// <param name="settings"></param>
    public GeneratingService(SettingsService<double> settings)
    {
        _settings = settings;
    }

    public double GenerateNumber()
    {
        return new Random().NextDouble() * (_settings.GetSettings().MaxNumber - _settings.GetSettings().MinNumber) + _settings.GetSettings().MinNumber;
    }
}
