﻿using GTN.Domain.Dto;

namespace GTN.Domain.Services.Integer;

public class DoubleSettingsService : SettingsService<double>
{
    public DoubleSettingsService(SettingsDto<double> defaultSettings) : base(defaultSettings)
    {
    }
}
