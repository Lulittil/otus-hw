﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
        int[] array = new int[1000000]; 
        Random rand = new Random();
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = rand.Next(1, 100);
        }

        Stopwatch stopwatch = new Stopwatch();

        // Обычное вычисление
        stopwatch.Start();
        long sum = 0;
        for (int i = 0; i < array.Length; i++)
        {
            sum += array[i];
        }
        stopwatch.Stop();
        Console.WriteLine("Обычное вычисление: " + sum + ", Время: " + stopwatch.ElapsedMilliseconds + "ms");

        // Параллельное вычисление
        stopwatch.Restart();
        long parallelSum = 0;
        Parallel.For(0, array.Length, i =>
        {
            parallelSum += array[i];
        });
        stopwatch.Stop();
        Console.WriteLine("Параллельное вычисление: " + parallelSum + ", Время: " + stopwatch.ElapsedMilliseconds + "ms");

        // Параллельное вычисление с помощью LINQ
        stopwatch.Restart();
        long linqSum = array.AsParallel().Sum();
        stopwatch.Stop();
        Console.WriteLine("Параллельное вычисление с помощью LINQ: " + linqSum + ", Время: " + stopwatch.ElapsedMilliseconds + "ms");
    }
}